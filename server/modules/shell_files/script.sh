#!/bin/bash 
#####################################################################
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#####################################################################
#
# ofbiz        This shell script takes care of starting and stopping
#              the OFBiz subsystem
#
# chkconfig: - 80 10
# description: OFBiz server
#. /etc/rc.d/init.d/functions

help1 () {
	echo "Running script in DEV environment"
	echo "1 for eclipse backend"
	echo "2 for community backend "
	echo "3 for rebalancer"
	echo "4 for redis"
	exit 0
}



if [ "$1" == "--help" ]; then
	help1
fi

ACTION=$1
THIRD=$2
TOMCAT_DIR_PATH=/usr/share/apache-tomcat-8.0.37/bin
ECLIPSE_NAME=oe-alpha
COMMUNITY_NAME=community_alpha

if [ "${ACTION}" == "" ]; then
	help1
fi



eclipseAction () {
#	echo "inside eclipse action"
	if [ "${ACTION}" == "status" ]; then
		STATUS="$(forever list | grep $ECLIPSE_NAME | awk '{print $9}')"
		if [ "$STATUS" == "STOPPED" -o "$STATUS" == "" ]; then
			exit 2;
		else	
			exit 0;
		fi
	else
		forever $ACTION $ECLIPSE_NAME
	fi
}

communityAction () {
#	echo "inside commnity action"
	if [ "${ACTION}" == "status" ]; then
		STATUS="$(forever list | grep $COMMUNITY_NAME | awk '{print $9}')"		
		if [  "$STATUS" == "STOPPED" -o "$STATUS" == "" ]; then
			echo "asdf"
			exit 2;
		else
			exit 0;
		fi
	else
		forever $ACTION $COMMUNITY_NAME
	fi
}

redisAction () {
#	echo "inside redis action"
	if [ "${ACTION}" == "status" ]; then
		STATUS="$(service redis $ACTION | awk '{print $3}')"
		if [ "$STATUS" == "stopped" ]; then
			exit 2;
		else
			exit 0;
		fi
	else
		service redis $ACTION;
	fi
}

rebalancerAction () {
#	echo "inside rebalacner action"
	if [ $ACTION == "start" ]; then
		sh $TOMCAT_DIR_PATH/startup.sh
	fi
	if [ $ACTION == "restart" ]; then
		sh $TOMCAT_DIR_PATH/shutdown.sh
		sh $TOMCAT_DIR_PATH/startup.sh
	fi
	if [ $ACTION == "stop" ]; then
		sh $TOMCAT_DIR_PATH/shutdown.sh
	fi
	STATUS=`/bin/ps h -o pid,args -C "java" | /bin/egrep -o "^[[:space:]]*[[:digit:]]*"`
	echo $STATUS
	if [  "$STATUS" == "" ]; then
		echo "asdf"
		exit 2;
	else
		exit 0;
	fi
}

if [ "${THIRD}" == "" ]; then
#	echo "Running all four application"
	eclipseAction
	communityAction
	redisAction
	rebalancerAction
fi


myArray=( "$@" )
COUNT=0
NUMBER=1
for var in "${myArray[@]}"; do
#	echo "singh"
	COUNT=$(($COUNT+$NUMBER))
#	echo "${var}"
#	echo "${COUNT}"
	if [ "${var}" == 1 ]; then
		#echo ""
		eclipseAction
	fi
	if [ "${var}" == 2 ]; then
                communityAction
        fi
	if [ "${var}" == 3 ]; then
                rebalancerAction
        fi
	if [ "${var}" == 4 ]; then
                redisAction
        fi
done


exit 0
